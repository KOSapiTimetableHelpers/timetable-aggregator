Viewer of weekly university timetable aggregated for a selected group of people
===========================================

About
-----

The purpose of this project is to simplify scheduling events for a large group of people considering their regular (weekly) timetable of classes. The data is loaded using [KOSapi-3](https://kosapi.fit.cvut.cz/) and thus it works at [Czech Technical University in Prague](https://www.cvut.cz/) only.


License
-------

Copyright Ondřej Guth, 2017, [Faculty of Information Technology](https://fit.cvut.cz) of [Czech Technical University in Prague](https://www.cvut.cz/).
