package cz.cvut.fit.kosapi2timetable.timetable_aggregator.timetable;

import static cz.cvut.fit.kosapi2timetable.timetable_aggregator.timetable.TimetableEvents.MAX_LESSON_START;
import static cz.cvut.fit.kosapi2timetable.timetable_aggregator.timetable.TimetableEvents.MIN_LESSON_START;
import java.time.DayOfWeek;
import java.util.Arrays;
import java.util.Collection;

/**
 * Timetable of multiple persons.
 * @author Ondrej Guth
 */
public class CombinedTimetable {
	final short [][] timetable = new short[7][MAX_LESSON_START-MIN_LESSON_START+1];
	public CombinedTimetable(final Collection<TimetableEvents> ts) {
		for (final TimetableEvents evs : ts) {
			for (DayOfWeek day : DayOfWeek.values()) {
				for (byte lesson = MIN_LESSON_START; lesson <= MAX_LESSON_START; lesson++) {
					if (evs.isBusy(day, lesson)) {
						timetable[day.getValue()-1][lesson-MIN_LESSON_START]++;
					}
				}
			}
		}
	}
	@Override
	public String toString() {
		return Arrays.deepToString(timetable);
	}
}
