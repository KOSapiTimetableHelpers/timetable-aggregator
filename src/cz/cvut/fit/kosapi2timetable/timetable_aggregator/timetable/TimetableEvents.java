package cz.cvut.fit.kosapi2timetable.timetable_aggregator.timetable;

import java.time.DayOfWeek;
import java.util.Arrays;

/**
 * Collection of all timetable events of a person or combination for multiple persons.
 * @author Ondrej Guth
 */
public class TimetableEvents {
	private short [] timetable = new short[7];
	/**
	 * Minimum starting lesson number.
	 */
	public static final byte MIN_LESSON_START = 1;
	
	/**
	 * Maximum starting lesson number.
	 */
	public static final byte MAX_LESSON_START = 15;

	/**
	 * Initialise empty instance.
	 */
	public TimetableEvents() {}
	
	/**
	 * Initialise this based on other collection of events. Makes a clone of other.
	 * @param other collection of events to take data from
	 */
	public TimetableEvents(final TimetableEvents other) {
		this.timetable = Arrays.copyOfRange(other.timetable, 0, other.timetable.length-1);
	}
	
	/**
	 * Add or set event of timetable.
	 * @param day day of week
	 * @param lesson lesson number (number 1-15)
	 * @throws IllegalArgumentException if lesson number is invalid
	 */
	public void setEvent(final DayOfWeek day, final byte lesson) {
		if (lesson < MIN_LESSON_START || lesson > MAX_LESSON_START) {
			throw new IllegalArgumentException(
					String.format("Lesson to set must be between %d and %d."
					, MIN_LESSON_START, MAX_LESSON_START));
		}
		timetable[day.getValue()-1] |= 1 << (lesson-1);
	}
	
	/**
	 * Add or set event of timetable.
	 * @param day day of week
	 * @param lesson lesson start number (number 1-15)
	 * @param length number of hours (45-min) of lesson
	 * @throws IllegalArgumentException if length is invalid (for the others see setEvent(final DayOfWeek day, final byte lesson)
	 */
	public void setEvent(final DayOfWeek day, final byte lesson, final byte length) {
		if (length < 1 || length > MAX_LESSON_START) {
			throw new IllegalArgumentException(
					String.format("Lesson to set must be between 1 and %d.", MAX_LESSON_START));
		}
		
		for(byte i = 0; i < length; i++) {
			setEvent(day, (byte)(lesson+i));
		}
	}
	
	/**
	 * Combine events of this and other collection. Does not modify this object.
	 * @param other other collection
	 * @return new collection containing events from this one and from other one (union of both)
	 */
	public TimetableEvents union(final TimetableEvents other) {
		final TimetableEvents result = new TimetableEvents();
		for (byte i = 0; i < timetable.length; i++) {
			result.timetable[i] = (short)(this.timetable[i] | other.timetable[i]);
		}
		return result;
	}
	
	public boolean isBusy(final DayOfWeek day, final byte lesson) {
		final byte mask = 0b1;
		final short daySched = timetable[day.getValue()-1];
		return ((daySched >>> (lesson-MIN_LESSON_START)) & mask) == 1;
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("  123456789ABCDEF\n");
		for (DayOfWeek day : DayOfWeek.values()) {
			sb.append("  ");
			for (byte lesson = MIN_LESSON_START; lesson <= MAX_LESSON_START; lesson++) {
				final boolean isOne = isBusy(day, lesson);
				sb.append(isOne ? "1" : "0");
			}
			sb.append('\n');
		}
		return sb.toString();
	}
}
