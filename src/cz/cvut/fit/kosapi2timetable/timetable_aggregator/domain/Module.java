package cz.cvut.fit.kosapi2timetable.timetable_aggregator.domain;

import cz.cvut.fit.kosapi2timetable.kosapi.DataExtractor;
import cz.cvut.fit.kosapi2timetable.timetable_aggregator.TimetableAggregator;
import java.util.Collection;
import java.util.Set;

/**
 * Purpose of this class is to extract parallels owned by this module.
 * @author Ondrej Guth
 */
public class Module {
    private final Set<Parallel> parallels;
    
    public Module(final String code) {
            final DataExtractor<Parallel> kosapiExtractorGlobal = new DataExtractor<>(
                    "/courses/" + code + "/parallels",
                    TimetableAggregator.SEMESTER,
                    "entry(id)",
                    "//*[local-name()='entry']",
                    null
            );
            
            parallels = kosapiExtractorGlobal.extractSet(s -> new Parallel(DataExtractor.extractKosId(s)));
    }
    
    public Collection<Parallel> getParallels() {
        return parallels;
    }
}
