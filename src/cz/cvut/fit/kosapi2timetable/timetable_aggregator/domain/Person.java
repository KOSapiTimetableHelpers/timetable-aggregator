package cz.cvut.fit.kosapi2timetable.timetable_aggregator.domain;

import cz.cvut.fit.kosapi2timetable.timetable_aggregator.TimetableAggregator;
import cz.cvut.fit.kosapi2timetable.timetable_aggregator.timetable.TimetableEvents;
import cz.cvut.fit.kosapi2timetable.kosapi.DataExtractor;
import cz.cvut.fit.kosapi2timetable.kosapi.UnauthorizedException;
import java.time.DayOfWeek;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Student or teacher.
 *
 * @author Ondrej Guth
 */
public class Person {

    /**
     * Combined student's and teacher's timetable.
     */
    private final TimetableEvents timetable = new TimetableEvents();
    private final String login;

    private void extractTimetable(final String url) {
        try {
            final DataExtractor<Void> deTeacherTimetable = new DataExtractor(url, TimetableAggregator.SEMESTER, "entry(content(timetableSlot))", "//timetableSlot", null);
            final Set<String> xpItems = new HashSet<>();
            xpItems.add("day/text()");
            xpItems.add("firstHour/text()");
            xpItems.add("duration/text()");
            final List<Map<String, String>> timetables = deTeacherTimetable.extractMulti(xpItems);
            for (final Map<String, String> lesson : timetables) {
                timetable.setEvent(
                        DayOfWeek.of(Integer.parseInt(lesson.get("day/text()"))), Byte.valueOf(lesson.get("firstHour/text()")), Byte.valueOf(lesson.get("duration/text()")));
            }
        } catch (final UnauthorizedException e) {
            throw e;
        } catch (RuntimeException ex) {
            System.err.println("Error for " + url + "\n" + ex.getCause());
        }
    }

    public Person(final String login) {
        this.login = login;
        extractTimetable("/teachers/" + login + "/parallels");
        extractTimetable("/students/" + login + "/parallels");
    }

    public TimetableEvents getTimetable() {
        return timetable;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 19 * hash + Objects.hashCode(this.login);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Person other = (Person) obj;
        return Objects.equals(this.login, other.login);
    }
}
