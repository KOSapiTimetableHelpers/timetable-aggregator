package cz.cvut.fit.kosapi2timetable.timetable_aggregator.domain;

import cz.cvut.fit.kosapi2timetable.timetable_aggregator.TimetableAggregator;
import cz.cvut.fit.kosapi2timetable.timetable_aggregator.timetable.TimetableEvents;
import cz.cvut.fit.kosapi2timetable.kosapi.DataExtractor;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author guthondr
 */
public class Parallel {

	private Set<Person> students;
	private final long id;

	private void parseAndLoadData() {
		students = new HashSet<>();
		final DataExtractor<Person> da = new DataExtractor("/parallels/" + id + "/students/", TimetableAggregator.SEMESTER, "entry(content(username))", "//username/text()", null);
//		final DataExtractor<Person> da = new DataExtractor("/courses/FI-MPL/students/?sem=B142", "//username/text()");
		students = da.extractSet(s -> new Person(s));
	}

	public Parallel(final long parallelId) {
		id = parallelId;
		parseAndLoadData();
	}
	
	public TimetableEvents getCombinedTimetable() {
		TimetableEvents result = new TimetableEvents();
		for (final Person student : students) {
			result = result.union(student.getTimetable());
		}
		return result;
	}
	
	public Set<Person> getStudents() {
		return students;
	}

	@Override
	public int hashCode() {
		int hash = 3;
		hash = 41 * hash + (int) (this.id ^ (this.id >>> 32));
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Parallel other = (Parallel) obj;
		if (this.id != other.id) {
			return false;
		}
		return true;
	}
	
	
}
