package cz.cvut.fit.kosapi2timetable.timetable_aggregator;

import cz.cvut.fit.kosapi2timetable.kosapi.AccessTokenRetriever;
import cz.cvut.fit.kosapi2timetable.kosapi.KosapiSettings;
import cz.cvut.fit.kosapi2timetable.kosapi.UnauthorizedException;
import cz.cvut.fit.kosapi2timetable.timetable_aggregator.domain.Module;
import cz.cvut.fit.kosapi2timetable.timetable_aggregator.domain.Parallel;
import cz.cvut.fit.kosapi2timetable.timetable_aggregator.domain.Person;
import cz.cvut.fit.kosapi2timetable.timetable_aggregator.timetable.CombinedTimetable;
import cz.cvut.fit.kosapi2timetable.timetable_aggregator.timetable.TimetableEvents;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import javax.swing.JOptionPane;

/**
 * Main class.
 *
 * @author Ondrej Guth
 */
public class TimetableAggregator {

    private static final String SETTINGS_FILENAME_DEFAULT = ".timetable_aggregator.properties";
    private static final File SETTINGS_FILE = new File(
            System.getProperty("cz.cvut.fit.kosapi2timetable.timetable_aggregator.settings_file", Paths.get(System.getProperty("user.home"), SETTINGS_FILENAME_DEFAULT).toString()));
    public static String SEMESTER = "B162";

    /**
     * Main method of this application.
     *
     * @param args command-line arguments: parallel ids and/or
     * teachers'/students' logins
     */
    public static void main(String[] args) throws IOException {
        final Properties settings = new Properties();
        settings.load(new FileInputStream(SETTINGS_FILE));
        KosapiSettings.getInstance().setSettings(settings);
        try {
        final Set<Parallel> selParallels = new HashSet<>();
        final Set<Person> selPersons = new HashSet<>();
        final String[] logins = System.getProperty("logins", "").split(",");
        final String[] parallelIds = System.getProperty("parallelIds", "").split(",");
        final String[] moduleCodes = System.getProperty("moduleCodes", "").split(",");
        Arrays.stream(logins).filter(i -> !i.isEmpty()).forEach(arg -> selPersons.add(new Person(arg)));
        Arrays.stream(parallelIds).filter(i -> !i.isEmpty()).forEach(arg -> selParallels.add(new Parallel(Long.valueOf(arg))));
        Arrays.stream(moduleCodes).filter(i -> !i.isEmpty()).forEach(arg -> selParallels.addAll(new Module(arg).getParallels()));
        selParallels.stream().forEach(p -> selPersons.addAll(p.getStudents()));
        final Set<TimetableEvents> evs = new HashSet<>(selPersons.size());
        selPersons.stream().forEach(p -> evs.add(p.getTimetable()));
        CombinedTimetable combSched = new CombinedTimetable(evs);
        System.out.println(combSched.toString());
        } catch (final UnauthorizedException e) {
                if (settings.getProperty(KosapiSettings.CLIENT_ID_PROP_NAME) == null) {
                    settings.setProperty(KosapiSettings.CLIENT_ID_PROP_NAME, JOptionPane.showInputDialog("Enter OAuth Client ID"));
                }
                if (settings.getProperty(KosapiSettings.CLIENT_SECRET_PROP_NAME) == null) {
                    settings.setProperty(KosapiSettings.CLIENT_SECRET_PROP_NAME, JOptionPane.showInputDialog("Enter OAuth Client secret"));
                }
                settings.setProperty(KosapiSettings.ACCESS_TOKEN_PROP_NAME, AccessTokenRetriever.retrieve());
                JOptionPane.showMessageDialog(null, "Authentication token retrieved, please rerun this application");
                settings.store(new FileOutputStream(SETTINGS_FILE), null);
                System.exit(2);
            }
    }
    //TODO parity (even/odd week)
}
